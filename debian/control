Source: lunar-date
Priority: optional
Maintainer: Debian Chinese Team <chinese-developers@lists.alioth.debian.org>
Uploaders:
 YunQiang Su <syq@debian.org>,
 ChangZhuo Chen (陳昌倬) <czchen@debian.org>,
 xiao sheng wen <atzlinux@sina.com>,
Build-Depends:
 meson,
 debhelper-compat (= 13),
 dh-exec,
 gnome-common,
 gobject-introspection,
 gtk-doc-tools,
 intltool,
 libgirepository1.0-dev,
 libglib2.0-dev,
 valac,
 dh-sequence-gir,
Rules-Requires-Root: no
Standards-Version: 4.6.0
Section: libs
Homepage: https://github.com/yetist/lunar-date
Vcs-Git: https://salsa.debian.org/chinese-team/lunar-date.git
Vcs-Browser: https://salsa.debian.org/chinese-team/lunar-date

Package: gir1.2-lunardate-3.0
Section: introspection
Multi-Arch: same
Architecture: any
Depends:
 ${gir:Depends},
 ${misc:Depends},
Description: GObject Introspection for lunar-date
 Lunar-date is a Chinese Lunar library based on GObject, which can
 covert between Chinese lunar calendar and Gregorian calendar.
 .
 This package contains GObject Introspection for lunar-date.

Package: liblunar-date-3.0-1
Pre-Depends:
 ${misc:Pre-Depends},
Multi-Arch: same
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Replaces: liblunar-date-2.0-0 (<< 3)
Breaks: liblunar-date-2.0-0 (<< 3)
Description: Chinese Lunar library based on GObject
 Lunar-date is a Chinese Lunar library based on GObject, which can
 covert between Chinese lunar calendar and Gregorian calendar.
 .
 This package contains shared library and vala bindings.

Package: liblunar-date-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 liblunar-date-3.0-1 (= ${binary:Version}),
 gir1.2-lunardate-3.0 (= ${binary:Version}),
 ${misc:Depends},
Suggests:
 liblunar-date-doc,
Description: Chinese Lunar library based on GObject - develop files
 Lunar-date is a Chinese Lunar library based on GObject, which can
 covert between Chinese lunar calendar and Gregorian calendar.
 .
 This package contains develop files:
  * C headers
  * pkgconfig lunar-date-3.0.pc
  * gir-1.0 LunarDate-3.0.gir

Package: liblunar-date-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Replaces: liblunar-date-dev (<< 3)
Breaks: liblunar-date-dev (<< 3)
Description: Chinese Lunar library based on GObject - API documents
 Lunar-date is a Chinese Lunar library based on GObject, which can
 covert between Chinese lunar calendar and Gregorian calendar.
 .
 This package contains API documents.

Package: lunar-date
Section: utils
Architecture: any
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 liblunar-date-3.0-1 (= ${binary:Version}),
Description: Chinese Lunar library based on GObject - command and D-Bus
 Lunar-date is a Chinese Lunar library based on GObject, which can
 covert between Chinese lunar calendar and Gregorian calendar.
 .
 This package contains lunar-date command, D-Bus org.chinese.Lunar.Date.service.
 The lunar-date need to call D-Bus when running.
